import time

from mdutils import MdUtils

from sklearn.model_selection import train_test_split

from SelfOrganizingMaps.SelfOrganizingMap import SelfOrganizingMap
from models.som.HebbianModel import HebbianModel
from oneShotLearning.precomputing_models import precompute_som_best_model, precompute_som
from oneShotLearning.utility import *
from oneShotLearning.clean_folders import clean_statistics_folders
from utils.constants import Constants
import matplotlib.pyplot as plt

if __name__ == '__main__':
    v_xs_all, v_ys_all, a_xs_all, a_ys_all, filenames_visual, filenames_audio = import_data(
        Constants.visual_data_path_segmented, Constants.audio_data_path, segmented=True)
    # a_xs_all, a_ys_all = one_hot_dataset()
    df_v = pd.read_csv(os.path.join(Constants.DATA_FOLDER, '100classes', 'VisualInputTrainingSet.csv'), header=None)
    df_v = df_v.drop(axis=1, columns=0)
    xs_v = df_v.to_numpy()[:1000]
    xs_chairs = xs_v[700:800]
    xs_table = v_xs_all[0:100]
    xs_lamp = xs_v[900:1000]
    xs_new = np.append(xs_table, xs_chairs, axis=0)
    v_xs_all = np.append(xs_new, v_xs_all[200:], axis=0)
    # print(xs_chairs)
    # print(np.shape(v_xs_all))
    v_dim, a_dim = np.shape(v_xs_all)[1], np.shape(a_xs_all)[1]
    df = pd.DataFrame(v_xs_all)
    # print(df.head())
    alpha, sigma, alpha_a, sigma_a = 0.3, 6, 0.4, 4
    n_iters, n_iters_a = 5500, 3500
    n, m = 25, 30
    scaler = 'Standard Scaler'
    splits_v, splits_a = 7, 3
    iterations_one_shot = 25
    iterations_one_shot_a = 30
    sigma_one_shot = 1.
    alpha_one_shot = 0.5
    hebbian_threshold = .80
    shots = 5
    # clean_statistics_folders()

    # Split dataset into training set and test set (balanced examples per class)
    v_xs_all_train, v_xs_all_test, v_ys_all_train, v_ys_all_test = train_test_split(v_xs_all, v_ys_all,
                                                                                    test_size=0.3, random_state=1,
                                                                                    stratify=v_ys_all)
    a_xs_all_train, a_xs_all_test, a_ys_all_train, a_ys_all_test = train_test_split(a_xs_all, a_ys_all,
                                                                                    test_size=0.3, random_state=1,
                                                                                    stratify=a_ys_all)
    path_root_models = os.path.join(Constants.trained_models_path, 'pre_one_shot_models')
    # precompute_som(v_xs_all_train, v_ys_all_train, a_xs_all_train, a_ys_all_train, n, m, n_iters, sigma, alpha,
    #                path_root_models)

    precompute_som_best_model(v_xs_all_train, v_ys_all_train, a_xs_all_train, a_ys_all_train, n, m, n_iters, sigma,
                              alpha, path_root_models, shots_hebbian_model=10)

    for shots in [10]:

        # Create a new dataset of n random examples per class used for hebbian training
        h_a_xs_train, h_a_ys_train, _, _ = get_random_classes(a_xs_all_train, a_ys_all_train,
                                                              Constants.classes, shots, -1)
        h_v_xs_train, h_v_ys_train, _, _ = get_random_classes(v_xs_all_train, v_ys_all_train,
                                                              Constants.classes, shots, -1)
        for hebbian_threshold in [85]:
            # for hebbian_threshold in range(60, 90, 5):

            hebbian_threshold = hebbian_threshold / 100

            for i in Constants.classes:
                if i == 0:
                    label_classes = Constants.label_classes.copy()
                    label_classes.pop(i)
                    classes = Constants.classes.copy()
                    classes.pop(i)
                    # Extract the class i-th from the train and the test sets
                    class_ext_v_xs, class_ext_v_ys, v_xs, v_ys = get_examples_of_class(v_xs_all_train, v_ys_all_train,
                                                                                       Constants.classes, i)
                    class_ext_a_xs, class_ext_a_ys, a_xs, a_ys = get_examples_of_class(a_xs_all_train, a_ys_all_train,
                                                                                       Constants.classes, i)
                    _, _, v_xs_n_1_classes_test, v_ys_n_1_classes_test = get_examples_of_class(v_xs_all_test,
                                                                                               v_ys_all_test,
                                                                                               Constants.classes, i)
                    _, _, a_xs_n_1_classes_test, a_ys_n_1_classes_test = get_examples_of_class(a_xs_all_test,
                                                                                               a_ys_all_test,
                                                                                               Constants.classes, i)

                    # Get n examples from the extracted class data for one-shot learning
                    class_ext_v_xs_train, class_ext_v_ys_train, class_ext_v_xs_test, class_ext_v_ys_test = get_random_classes(
                        class_ext_v_xs, class_ext_v_ys, [i], shots, -1)
                    class_ext_a_xs_train, class_ext_a_ys_train, class_ext_a_xs_test, class_ext_a_ys_test = get_random_classes(
                        class_ext_a_xs, class_ext_a_ys, [i], shots, -1)

                    # Get 10 examples from the n-1 classes, used to train the hebbian model for SOMs pre one-shot
                    h_a_xs_train_n_1, h_a_ys_train_n_1, h_a_xs_test_n_1, h_a_ys_test_n_1 = get_random_classes(a_xs,
                                                                                                              a_ys,
                                                                                                              Constants.classes,
                                                                                                              shots, -1,
                                                                                                              class_to_exclude=i)
                    h_v_xs_train_n_1, h_v_ys_train_n_1, h_v_xs_test_n_1, h_v_ys_test_n_1 = get_random_classes(v_xs,
                                                                                                              v_ys,
                                                                                                              Constants.classes,
                                                                                                              shots, -1,
                                                                                                              class_to_exclude=i)

                    print(
                        f'\n\n ++++++++++++++++++++++++++++ {i + 1}) TEST ONE SHOT ON CLASS = \'{Constants.label_classes[i]}\' ++++++++++++++++++++++++++++ \n\n')

                    iterations = 1
                    accuracy_list, accuracy_list_source_a = [], []
                    precisions, precisions_a = [], []
                    recalls, recalls_a = [], []
                    f1_scores, f1_scores_a = [], []

                #     som_v = SelfOrganizingMap(n, m, v_dim, n_iterations=n_iters, sigma=sigma, learning_rate=alpha)
                #     som_a = SelfOrganizingMap(n, m, a_dim, n_iterations=n_iters_a, sigma=sigma_a, learning_rate=alpha)
                #
                #     # SCOMMENTARE PER CARICARE MODELLI PRECEDENTI
                #     # if check_trained_models(trained_models_path):
                #     #     print('Restoring Precomputed Models...')
                #     #     weights_v, weights_a = get_trained_model(trained_models_path)
                #     #     som_v.restore_train(weights_v, v_xs, v_ys)
                #     #     som_a.restore_train(weights_a, a_xs, a_ys)
                #     # else:
                #     #     weights_v, weights_a, som_v, som_a = train_som_and_get_weight(som_v, som_a, v_xs, a_xs, v_ys,
                #     #                                                                   a_ys)
                #     weights_v, weights_a, som_v, som_a = train_som_and_get_weight(som_v, som_a, v_xs, a_xs, v_ys,
                #                                                                   a_ys)
                #
                #     hebbian_model = HebbianModel(som_a, som_v, a_dim, v_dim, n_presentations=shots, learning_rate=10,
                #                                  n_classes=len(Constants.classes) - 1, threshold=hebbian_threshold)
                #     hebbian_model.train(h_a_xs_train_n_1, h_v_xs_train_n_1)
                #     print('Class, Accuracy train v, Accuracy train a, Accuracy test v, Accuracy test a')
                #     for index_class in Constants.classes:
                #         # start = time.process_time()
                #         print('--> Evaluating Activation for class \'{}\''.format(Constants.label_classes[index_class]))
                #
                #         accuracy_train_v = hebbian_model.evalute_class(a_xs_all_train,
                #                                                        v_xs_all_train,
                #                                                        a_ys_all_train,
                #                                                        v_ys_all_train,
                #                                                        source='v',
                #                                                        class_to_evaluate=index_class)
                #         accuracy_train_a = hebbian_model.evalute_class(a_xs_all_train,
                #                                                        v_xs_all_train,
                #                                                        a_ys_all_train,
                #                                                        v_ys_all_train,
                #                                                        source='a',
                #                                                        class_to_evaluate=index_class)
                #         accuracy_test_v = hebbian_model.evalute_class(a_xs_all_test, v_xs_all_test,
                #                                                       a_ys_all_test,
                #                                                       v_ys_all_test, source='v',
                #                                                       class_to_evaluate=index_class)
                #         accuracy_test_a = hebbian_model.evalute_class(a_xs_all_test, v_xs_all_test,
                #                                                       a_ys_all_test,
                #                                                       v_ys_all_test, source='a',
                #                                                       class_to_evaluate=index_class)
                #         print(
                #             f'{Constants.label_classes[index_class]}, {accuracy_train_v}, {accuracy_train_a}, {accuracy_test_v}, {accuracy_test_a}')
                # ###############################

                som_v_all = SelfOrganizingMap(n, m, v_dim, n_iterations=n_iters, sigma=sigma, learning_rate=alpha)
                som_a_all = SelfOrganizingMap(n, m, a_dim, n_iterations=n_iters_a, sigma=sigma_a, learning_rate=alpha_a)
                weights_v, weights_a, som_v_all, som_a_all = train_som_and_get_weight(som_v_all, som_a_all,
                                                                                      v_xs_all_train,
                                                                                      a_xs_all_train,
                                                                                      v_ys_all_train, a_ys_all_train)

                hebbian_model = HebbianModel(som_a_all, som_v_all, a_dim, v_dim, n_presentations=shots, learning_rate=10,
                                             n_classes=len(Constants.classes), threshold=hebbian_threshold)
                hebbian_model.train(h_a_xs_train, h_v_xs_train)
                print('#############')
                print('Class, Accuracy train v, Accuracy train a, Accuracy test v, Accuracy test a')
                for index_class in Constants.classes:
                    # start = time.process_time()
                    print('--> Evaluating Activation for class \'{}\''.format(Constants.label_classes[index_class]))

                    accuracy_train_v = hebbian_model.evalute_class(a_xs_all_train,
                                                                   v_xs_all_train,
                                                                   a_ys_all_train,
                                                                   v_ys_all_train,
                                                                   source='v',
                                                                   class_to_evaluate=index_class)
                    accuracy_train_a = hebbian_model.evalute_class(a_xs_all_train,
                                                                   v_xs_all_train,
                                                                   a_ys_all_train,
                                                                   v_ys_all_train,
                                                                   source='a',
                                                                   class_to_evaluate=index_class)
                    accuracy_test_v = hebbian_model.evalute_class(a_xs_all_test, v_xs_all_test,
                                                                  a_ys_all_test,
                                                                  v_ys_all_test, source='v',
                                                                  class_to_evaluate=index_class)
                    accuracy_test_a = hebbian_model.evalute_class(a_xs_all_test, v_xs_all_test,
                                                                  a_ys_all_test,
                                                                  v_ys_all_test, source='a',
                                                                  class_to_evaluate=index_class)
                    print(
                        f'{Constants.label_classes[index_class]}, {accuracy_train_v}, {accuracy_train_a}, {accuracy_test_v}, {accuracy_test_a}')

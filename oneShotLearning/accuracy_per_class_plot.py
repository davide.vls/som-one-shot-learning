import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from utils.constants import Constants

if __name__ == '__main__':
    root_path = os.path.join(Constants.PLOT_FOLDER, 'temp')
    class_test = 'table'
    results = []
    shots = [1, 3, 5, 7, 10, 12, 15]
    for shot in shots:
        print(f'################################# shot n = {shot}')
        results_shot = []
        for exec_index in range(0, 6):
            exec_path = os.path.join(root_path, f'exec_pt2_{exec_index}_{class_test}')

            inner_path = os.path.join(exec_path, f'{shot}_shot_accuracies', 'results_threshold_85',
                                      f'1_one_shot_class_{class_test}', 'accuracies_per_class.csv')
            df = pd.read_csv(inner_path)
            results_shot.append(df.to_numpy()[0][2:])
        # print(results_shot)
        results.append(np.mean(results_shot, axis=0) * 100)
    results = np.array(results).T
    print(results)
    fig, ax = plt.subplots()
    fig.set_size_inches(10, 7)
    ax.plot(shots, results[0], linewidth=3, color='#E76F51', linestyle='--', alpha=0.5)
    ax.plot(shots, results[2], linewidth=3, color='#2A9D8F')

    plt.xticks(np.arange(1, shots[-1] + 1, 1))
    plt.yticks(np.arange(0, 105, 5))
    ax.set(xlabel='# of joint presentation', ylabel='Class Accuracy (%)',
           title='Accuracies on Class Table after one-shot-learning (Source \'Video\')')
    ax.grid(axis='y', linewidth=0.5, alpha=0.5)

    plt.legend(['Accuracy Training Set OneShot (Source \'Visual\')', 'Accuracy Test Set OneShot (Source \'Visual\')'])
    plt.savefig('accuracies_table_one_shot_v.png')
    plt.close(fig)
    ###################

    fig, ax = plt.subplots()
    fig.set_size_inches(10, 7)
    ax.plot(shots, results[1], linewidth=3, color='#0075C4', linestyle='--', alpha=0.5)
    ax.plot(shots, results[3], linewidth=3, color='#FCA311')

    plt.xticks(np.arange(1, shots[-1] + 1, 1))
    plt.yticks(np.arange(0, 105, 5))
    ax.set(xlabel='# of joint presentation', ylabel='Class Accuracy (%)',
           title='Accuracies on Class Table after one-shot-learning (Source \'Audio\')')
    ax.grid(axis='y', linewidth=0.5, alpha=0.5)

    plt.legend(['Accuracy Training Set OneShot (Source \'Audio\')', 'Accuracy Test Set OneShot (Source \'Audio\')'])
    plt.savefig('accuracies_table_one_shot_a.png')

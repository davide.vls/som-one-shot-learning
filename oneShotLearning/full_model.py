import os
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

from SelfOrganizingMap import SelfOrganizingMap
from models.som.HebbianModel import HebbianModel
from oneShotLearning.utility import import_data, train_som_and_get_weight, get_random_classes, safe_create_folder
from utils.constants import Constants

if __name__ == '__main__':
    v_xs_all, v_ys_all, a_xs_all, a_ys_all, filenames_visual, filenames_audio = import_data(
        Constants.visual_data_path_segmented, Constants.audio_data_path, segmented=True)
    # a_xs_all, a_ys_all = one_hot_dataset()
    df_v = pd.read_csv(os.path.join(Constants.DATA_FOLDER, '100classes', 'VisualInputTrainingSet.csv'), header=None)
    df_v = df_v.drop(axis=1, columns=0)
    xs_v = df_v.to_numpy()[:1000]
    xs_chairs = xs_v[700:800]
    xs_table = v_xs_all[0:100]
    xs_lamp = xs_v[900:1000]
    xs_new = np.append(xs_table, xs_chairs, axis=0)
    v_xs_all = np.append(xs_new, v_xs_all[200:], axis=0)
    # Split dataset into training set and test set (balanced examples per class)
    v_xs_all_train, v_xs_all_test, v_ys_all_train, v_ys_all_test = train_test_split(v_xs_all, v_ys_all,
                                                                                    test_size=0.3, random_state=1,
                                                                                    stratify=v_ys_all)
    a_xs_all_train, a_xs_all_test, a_ys_all_train, a_ys_all_test = train_test_split(a_xs_all, a_ys_all,
                                                                                    test_size=0.3, random_state=1,
                                                                                    stratify=a_ys_all)

    v_dim, a_dim = np.shape(v_xs_all)[1], np.shape(a_xs_all)[1]
    alpha, sigma, alpha_a, sigma_a = 0.3, 6, 0.4, 3.5
    n_iters, n_iters_a = 5500, 3500
    n, m = 25, 30
    scaler = 'Standard Scaler'
    splits_v, splits_a = 7, 3
    iterations_one_shot = 25
    iterations_one_shot_a = 30
    sigma_one_shot = 1.
    alpha_one_shot = 0.5
    hebbian_threshold = .80
    shots = 10

    h_a_xs_train, h_a_ys_train, h_a_xs_test, h_a_ys_test = get_random_classes(a_xs_all_train, a_ys_all_train,
                                                                              Constants.classes, shots, -1)
    h_v_xs_train, h_v_ys_train, h_v_xs_test, h_v_ys_test = get_random_classes(v_xs_all_train, v_ys_all_train,
                                                                              Constants.classes, shots, -1)
    max_weights_v, max_weights_a = None, None
    max_score = 0
    best_accuracy_test_a, best_accuracy_test_v, best_accuracy_train_v, best_accuracy_train_a = 0, 0, 0, 0
    for i in range(0, 5):
        print(f'---------- Model {i + 1} ----------')
        som_v = SelfOrganizingMap(n, m, v_dim, n_iterations=n_iters, sigma=sigma, learning_rate=alpha)
        som_a = SelfOrganizingMap(n, m, a_dim, n_iterations=n_iters_a, sigma=sigma_a, learning_rate=alpha_a)
        weights_v, weights_a, som_v, som_a = train_som_and_get_weight(som_v, som_a, v_xs_all_train, a_xs_all_train,
                                                                      v_ys_all_train, a_ys_all_train)
        score = 0
        best_actual_score = 0
        best_actual_accuracy_test_a, best_actual_accuracy_test_v, best_actual_accuracy_train_v, best_actual_accuracy_train_a = 0, 0, 0, 0
        for j in range(0, 5):
            print(f'++++++++++++ Hebbian Model {j + 1} of model {i + 1} ++++++++++++')
            hebbian_model = HebbianModel(som_a, som_v, a_dim, v_dim, n_presentations=shots, learning_rate=10,
                                         n_classes=len(Constants.classes), threshold=hebbian_threshold)
            hebbian_model.train(h_a_xs_train, h_v_xs_train)

            # Evaluating Hebbian Model for SOMs with n-1 classes
            accuracy_test_v = hebbian_model.evaluate(a_xs_all_test, v_xs_all_test, a_ys_all_test,
                                                     v_ys_all_test, source='v', prediction_alg='regular')
            accuracy_test_a = hebbian_model.evaluate(a_xs_all_test, v_xs_all_test, a_ys_all_test,
                                                     v_ys_all_test, source='a', prediction_alg='regular')
            accuracy_train_v = hebbian_model.evaluate(a_xs_all_train, v_xs_all_train, a_ys_all_train,
                                                      v_ys_all_train, source='v', prediction_alg='regular')
            accuracy_train_a = hebbian_model.evaluate(a_xs_all_train, v_xs_all_train, a_ys_all_train,
                                                      v_ys_all_train, source='a', prediction_alg='regular')
            print(f'Accuracy Test Set source audio (alg_pred = regular) = {accuracy_test_a}')
            print(f'Accuracy Test Set source video (alg_pred = regular) = {accuracy_test_v}')
            print(f'Accuracy Training Set source audio (alg_pred = regular) = {accuracy_train_a}')
            print(f'Accuracy Training Set source video (alg_pred = regular) = {accuracy_train_v}')
            actual_score = accuracy_test_a + accuracy_test_v + accuracy_train_v + accuracy_train_a
            score = score + actual_score
            if actual_score > best_actual_score:
                best_actual_score = actual_score
                best_actual_accuracy_test_a, best_actual_accuracy_test_v, \
                best_actual_accuracy_train_v, best_actual_accuracy_train_a = accuracy_test_a, accuracy_test_v, \
                                                                             accuracy_train_v, accuracy_train_a
        if score > max_score:
            max_score = score
            max_weights_a, max_weights_v = weights_a, weights_v
            best_accuracy_test_a, best_accuracy_test_v, \
            best_accuracy_train_v, best_accuracy_train_a = best_actual_accuracy_test_a, best_actual_accuracy_test_v, \
                                                           best_actual_accuracy_train_v, best_actual_accuracy_train_a

    path = os.path.join(Constants.trained_models_path, 'full_model')
    # results = {'Accuracy Test Set source audio': [best_accuracy_test_a],
    #            'Accuracy Test Set source video': [best_accuracy_test_v],
    #            'Accuracy Training Set source audio': [best_accuracy_train_a],
    #            'Accuracy Training Set source video': [best_accuracy_train_v]}
    results = [['Accuracy Test Set source audio', 'Accuracy Test Set source video',
                'Accuracy Training Set source audio', 'Accuracy Training Set source video'],
               [best_accuracy_test_a, best_accuracy_test_v, best_accuracy_train_a, best_accuracy_train_v]]
    safe_create_folder(path)
    file_path_som_v = os.path.join(path, 'som_v_model.npy')
    file_path_som_a = os.path.join(path, 'som_a_model.npy')
    np.save(file_path_som_v, max_weights_v)
    np.save(file_path_som_a, max_weights_a)
    df = pd.DataFrame(results)
    df.to_csv(os.path.join(path, 'accuracies.csv'), header=None)

import os
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

from SelfOrganizingMap import SelfOrganizingMap
from models.som.HebbianModel import HebbianModel
from oneShotLearning.utility import import_data, train_som_and_get_weight, get_random_classes, safe_create_folder
from utils.constants import Constants

if __name__ == '__main__':
    v_xs_all, v_ys_all, a_xs_all, a_ys_all, filenames_visual, filenames_audio = import_data(
        Constants.visual_data_path_segmented, Constants.audio_data_path, segmented=True)
    # a_xs_all, a_ys_all = one_hot_dataset()
    df_v = pd.read_csv(os.path.join(Constants.DATA_FOLDER, '100classes', 'VisualInputTrainingSet.csv'), header=None)
    df_v = df_v.drop(axis=1, columns=0)
    xs_v = df_v.to_numpy()[:1000]
    xs_chairs = xs_v[700:800]
    xs_table = v_xs_all[0:100]
    xs_lamp = xs_v[900:1000]
    xs_new = np.append(xs_table, xs_chairs, axis=0)
    v_xs_all = np.append(xs_new, v_xs_all[200:], axis=0)
    # Split dataset into training set and test set (balanced examples per class)
    v_xs_all_train, v_xs_all_test, v_ys_all_train, v_ys_all_test = train_test_split(v_xs_all, v_ys_all,
                                                                                    test_size=0.3, random_state=1,
                                                                                    stratify=v_ys_all)
    a_xs_all_train, a_xs_all_test, a_ys_all_train, a_ys_all_test = train_test_split(a_xs_all, a_ys_all,
                                                                                    test_size=0.3, random_state=1,
                                                                                    stratify=a_ys_all)

    v_dim, a_dim = np.shape(v_xs_all)[1], np.shape(a_xs_all)[1]
    print(np.shape(v_xs_all), np.shape(a_xs_all))
    alpha, sigma, alpha_a, sigma_a = 0.3, 6, 0.4, 3.5
    n_iters, n_iters_a = 5500, 3500
    n, m = 25, 30
    scaler = 'Standard Scaler'
    splits_v, splits_a = 7, 3
    iterations_one_shot = 25
    iterations_one_shot_a = 30
    sigma_one_shot = 1.
    alpha_one_shot = 0.5
    hebbian_threshold = .80
    shots = 10

    h_a_xs_train, h_a_ys_train, h_a_xs_test, h_a_ys_test = get_random_classes(a_xs_all_train, a_ys_all_train,
                                                                              Constants.classes, shots, -1)
    h_v_xs_train, h_v_ys_train, h_v_xs_test, h_v_ys_test = get_random_classes(v_xs_all_train, v_ys_all_train,
                                                                              Constants.classes, shots, -1)

    som_v = SelfOrganizingMap(n, m, v_dim, n_iterations=n_iters, sigma=sigma, learning_rate=alpha)
    som_a = SelfOrganizingMap(n, m, a_dim, n_iterations=n_iters_a, sigma=sigma_a, learning_rate=alpha_a)
    weights_v = np.load('./trained_models/full_model/som_v_model.npy')
    weights_a = np.load('./trained_models/full_model/som_a_model.npy')
    print(np.shape(weights_a))
    som_v.restore_train(weights_v, v_xs_all_train, v_ys_all_train)
    som_a.restore_train(weights_a, a_xs_all_train, a_ys_all_train)
    #
    som_v.plot_som(v_xs_all_train, v_ys_all_train, './', type_dataset='train_v')
    som_a.plot_som(a_xs_all_train, a_ys_all_train, './', type_dataset='train_a')
    som_v.plot_som(v_xs_all_test, v_ys_all_test, './', type_dataset='test_v')
    som_a.plot_som(a_xs_all_test, a_ys_all_test, './', type_dataset='test_a')

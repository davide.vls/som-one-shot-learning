import os
import pandas as pd
import numpy as np

from oneShotLearning.utility import import_data
from utils.constants import Constants
from oneShotLearning.utility import prototype_distances

if __name__ == '__main__':
    v_xs_all, v_ys_all, a_xs_all, a_ys_all, filenames_visual, filenames_audio = import_data(
        Constants.visual_data_path_segmented, Constants.audio_data_path, segmented=True)

    df_v = pd.read_csv(os.path.join(Constants.DATA_FOLDER, '100classes', 'VisualInputTrainingSet.csv'), header=None)
    df_v = df_v.drop(axis=1, columns=0)
    xs_v = df_v.to_numpy()[:1000]
    xs_chairs = xs_v[700:800]
    xs_table = v_xs_all[0:100]
    xs_lamp = xs_v[900:1000]
    xs_new = np.append(xs_table, xs_chairs, axis=0)
    v_xs_all = np.append(xs_new, v_xs_all[200:], axis=0)
    distances_v = prototype_distances(v_xs_all, v_ys_all)
    distances_a = prototype_distances(a_xs_all, a_ys_all)
    df_distances_v = pd.DataFrame(distances_v)
    df_distances_a = pd.DataFrame(distances_a)
    print(df_distances_v)
    print(df_distances_a)
    df_distances_v.to_latex('latex_v.tex')
    df_distances_a.to_latex('latex_a.tex')


import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from utils.constants import Constants
from oneShotLearning.utility import safe_create_folder
from scipy.interpolate import make_interp_spline, BSpline


def plot_accuracies(result_n_1_classes, result_one_shot, shots, title, image_path, full_model_accuracy):
    result_one_shot = [float(element) * 100 for element in result_one_shot]
    result_n_1_classes = [float(result_n_1_classes[4]) * 100] * len(
        result_n_1_classes)
    result_full_model = [float(full_model_accuracy) * 100] * len(
        result_n_1_classes)
    # plt.style.use(u'seaborn-bright')
    fig, ax = plt.subplots()
    fig.set_size_inches(10, 7)
    ax.plot(shots, result_n_1_classes, linewidth=3, color='#E76F51')
    ax.plot(shots, result_one_shot, linewidth=3, color='#FCA311')
    ax.plot(shots, result_full_model, linewidth=3, color='#2A9D8F')

    plt.xticks(np.arange(1, shots[-1] + 1, 1))
    plt.yticks(np.arange(30, 105, 5))
    ax.set(xlabel='# of joint presentation', ylabel='Mean Accuracy (%)',
           title=title)
    ax.grid(axis='y', linewidth=0.5, alpha=0.5)

    plt.legend(['Accuracies SOM n-1 classes', 'Accuracies SOM One Shot', 'Accuracies SOM all classes'],
               loc='bottom right')
    plt.savefig(image_path)
    # plt.show()
    plt.close()


if __name__ == '__main__':
    # thresholds = range(60, 90, 5)
    threshold = 85
    shots = [1, 3, 5, 7, 10, 12, 15]
    results_train_v, results_train_a = [], []
    results_test_v, results_test_a = [], []
    results_train_v_one_shot, results_train_a_one_shot = [], []
    results_test_v_one_shot, results_test_a_one_shot = [], []
    for class_index in Constants.classes:
        class_label = Constants.label_classes[class_index]
        result_train_v, result_train_a = [], []
        result_test_v, result_test_a = [], []
        result_train_v_one_shot, result_train_a_one_shot = [], []
        result_test_v_one_shot, result_test_a_one_shot = [], []
        print(f'-------------- class {class_label} ------------------')
        for shot in shots:
            root_folder = os.path.join(Constants.statistics_path, 'final_tests', f'{shot}_shot_accuracies',
                                       f'results_threshold_{threshold}')
            with open(
                    os.path.join(root_folder, f'{class_index + 1}_one_shot_class_{class_label}',
                                 f'results_class_{class_label}.md')) as f:
                file_split = [line.split('\n') for line in f]
                file_split = [line[0][1:-1].split('|') for line in file_split if 'Accuracy' in line[0]]
                for element in file_split:
                    if 'Training' in element[0]:
                        if 'Audio' in element[0]:
                            if 'One-Shot' in element[0]:
                                result_train_a_one_shot.append(element[1])
                            else:
                                result_train_a.append(element[1])
                        else:
                            if 'One-Shot' in element[0]:
                                result_train_v_one_shot.append(element[1])
                            else:
                                result_train_v.append(element[1])
                    else:
                        if 'Audio' in element[0]:
                            if 'One-Shot' in element[0]:
                                result_test_a_one_shot.append(element[1])
                            else:
                                result_test_a.append(element[1])
                        else:
                            if 'One-Shot' in element[0]:
                                result_test_v_one_shot.append(element[1])
                            else:
                                result_test_v.append(element[1])
        path_training = os.path.join(Constants.statistics_path, 'final_tests', 'result_accuracies_training_set')
        path_test = os.path.join(Constants.statistics_path, 'final_tests', 'result_accuracies_test_set')
        safe_create_folder(path_training)
        safe_create_folder(path_test)
        # df = pd.read_csv(os.path.join(Constants.trained_models_path, 'full_model', 'accuracies.csv'))
        # df = df.drop(columns='0', axis=1)
        # first_row = df.iloc[0].to_numpy()

        results_train_v.append(result_train_v)
        results_train_a.append(result_train_a)
        results_test_v.append(result_test_v)
        results_test_a.append(result_test_a)
        results_train_v_one_shot.append(result_train_v_one_shot)
        results_train_a_one_shot.append(result_train_a_one_shot)
        results_test_v_one_shot.append(result_test_v_one_shot)
        results_test_a_one_shot.append(result_test_a_one_shot)
        # plot_accuracies(result_test_a, result_test_a_one_shot, shots,
        #                 f'Accuracies on Test Set adding class \"{class_label}\" to model (Source \'Audio\')',
        #                 os.path.join(path_test, f'{class_index + 1}_accuracies_class_{class_label}_source_a.png'),
        #                 first_row[0])
        # plot_accuracies(result_test_v, result_test_v_one_shot, shots,
        #                 f'Accuracies on Test Set adding class \"{class_label}\" to model (Source \'Visual\')',
        #                 os.path.join(path_test, f'{class_index + 1}_accuracies_class_{class_label}_source_v.png'),
        #                 first_row[1])
        # plot_accuracies(result_train_a, result_train_a_one_shot, shots,
        #                 f'Accuracies on Training Set adding class {class_label} to model (Source \'Audio\')',
        #                 os.path.join(path_training, f'{class_index + 1}_accuracies_class_{class_label}_source_a.png'),
        #                 first_row[2])
        # plot_accuracies(result_train_v, result_train_v_one_shot, shots,
        #                 f'Accuracies on Training Set adding class {class_label} to model (Source \'Visual\')',
        #                 os.path.join(path_training, f'{class_index + 1}_accuracies_class_{class_label}_source_v.png'),
        #                 first_row[3])
    # print(np.array(results_test_a))
    # np.mean(np.array(results_test_a).astype(np.float), axis=0)
    results_train_v, results_train_a = np.mean(np.array(results_train_v).astype(np.float), axis=0), \
                                       np.mean(np.array(results_train_a).astype(np.float), axis=0)
    results_test_v, results_test_a = np.mean(np.array(results_test_v).astype(np.float), axis=0), \
                                     np.mean(np.array(results_test_a).astype(np.float), axis=0)
    results_train_v_one_shot, results_train_a_one_shot = np.mean(np.array(results_train_v_one_shot).astype(np.float),
                                                                 axis=0), \
                                                         np.mean(np.array(results_train_a_one_shot).astype(np.float),
                                                                 axis=0)
    results_test_v_one_shot, results_test_a_one_shot = np.mean(np.array(results_test_v_one_shot).astype(np.float),
                                                               axis=0), \
                                                       np.mean(np.array(results_test_a_one_shot).astype(np.float),
                                                               axis=0)
    # print(results_test_a_one_shot)
    df = pd.read_csv(os.path.join(Constants.trained_models_path, 'full_model', 'accuracies.csv'))
    df = df.drop(columns='0', axis=1)
    first_row = df.iloc[0].to_numpy()

    plot_accuracies(results_test_a, results_test_a_one_shot, shots,
                    f'Mean accuracies on Test Set (Source \'Audio\')',
                    os.path.join(Constants.statistics_path, 'final_tests', f'mean_accuracies_test_set_source_a.png'),
                    first_row[0])
    plot_accuracies(results_test_v, results_test_v_one_shot, shots,
                    f'Mean accuracies on Test Set (Source \'Video\')',
                    os.path.join(Constants.statistics_path, 'final_tests', f'mean_accuracies_test_set_source_v.png'),
                    first_row[1])
    plot_accuracies(results_train_a, results_train_a_one_shot, shots,
                    f'Mean accuracies on Training Set (Source \'Audio\')',
                    os.path.join(Constants.statistics_path, 'final_tests', f'mean_accuracies_training_set_source_a.png'),
                    first_row[2])
    plot_accuracies(results_train_v, results_train_v_one_shot, shots,
                    f'Mean accuracies on Training Set (Source \'Video\')',
                    os.path.join(Constants.statistics_path, 'final_tests', f'mean_accuracies_training_set_source_v.png'),
                    first_row[3])

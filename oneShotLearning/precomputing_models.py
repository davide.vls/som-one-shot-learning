import os
import  numpy as np

from SelfOrganizingMap import SelfOrganizingMap
from models.som.HebbianModel import HebbianModel
from oneShotLearning.utility import safe_create_folder, get_examples_of_class, train_som_and_get_weight, \
    get_random_classes
from utils.constants import Constants


def precompute_som(v_xs_train, v_ys_train, a_xs_train, a_ys_train, n, m, n_iters, sigma, alpha, path):
    # path = os.path.join(Constants.trained_models_path)
    if not os.path.exists(path):
        safe_create_folder(path)
    # nested_pre_one_shot_folder = os.path.join(path, 'pre_one_shot_models')
    # safe_create_folder(nested_pre_one_shot_folder)
    files = os.listdir(path)
    v_dim, a_dim = np.shape(v_xs_train)[1], np.shape(a_xs_train)[1]
    if len(files) < len(Constants.classes):
        for class_index in Constants.classes:
            print(f'*** Computing Model for {Constants.label_classes[class_index]} ***')
            inner_path = os.path.join(path, f'pre_one_shot_{Constants.label_classes[class_index]}')
            safe_create_folder(inner_path)
            _, _, v_xs, v_ys = get_examples_of_class(v_xs_train, v_ys_train, Constants.classes, class_index)
            _, _, a_xs, a_ys = get_examples_of_class(a_xs_train, a_ys_train, Constants.classes, class_index)
            som_v = SelfOrganizingMap(n, m, v_dim, n_iterations=n_iters, sigma=sigma, learning_rate=alpha)
            som_a = SelfOrganizingMap(n, m, a_dim, n_iterations=n_iters, sigma=sigma, learning_rate=alpha)
            weights_v, weights_a, _, _ = train_som_and_get_weight(som_v, som_a, v_xs, a_xs, v_ys, a_ys)
            file_path_som_v = os.path.join(inner_path, 'som_v_model.npy')
            file_path_som_a = os.path.join(inner_path, 'som_a_model.npy')
            np.save(file_path_som_v, weights_v)
            np.save(file_path_som_a, weights_a)


def precompute_som_best_model(v_xs_train, v_ys_train, a_xs_train, a_ys_train, n, m, n_iters, sigma, alpha, path,
                              n_iterations_models=4, shots_hebbian_model=10, hebbian_threshold=.75):
    # path = os.path.join(Constants.trained_models_path)
    if not os.path.exists(path):
        safe_create_folder(path)
    # nested_pre_one_shot_folder = os.path.join(path, 'pre_one_shot_models')
    # safe_create_folder(nested_pre_one_shot_folder)
    files = os.listdir(path)
    v_dim, a_dim = np.shape(v_xs_train)[1], np.shape(a_xs_train)[1]
    if len(files) < len(Constants.classes):
        for class_index in Constants.classes:
            print(f'*** Computing Model for {Constants.label_classes[class_index]} ***')
            inner_path = os.path.join(path, f'pre_one_shot_{Constants.label_classes[class_index]}')
            safe_create_folder(inner_path)
            _, _, v_xs, v_ys = get_examples_of_class(v_xs_train, v_ys_train, Constants.classes, class_index)
            _, _, a_xs, a_ys = get_examples_of_class(a_xs_train, a_ys_train, Constants.classes, class_index)
            # Get 10 examples from the n-1 classes, used to train the hebbian model for SOMs pre one-shot
            h_a_xs_train_n_1, h_a_ys_train_n_1, h_a_xs_test_n_1, h_a_ys_test_n_1 = get_random_classes(a_xs, a_ys,
                                                                                                      Constants.classes,
                                                                                                      shots_hebbian_model,
                                                                                                      -1,
                                                                                                      class_to_exclude=class_index)
            h_v_xs_train_n_1, h_v_ys_train_n_1, h_v_xs_test_n_1, h_v_ys_test_n_1 = get_random_classes(v_xs, v_ys,
                                                                                                      Constants.classes,
                                                                                                      shots_hebbian_model,
                                                                                                      -1,
                                                                                                      class_to_exclude=class_index)

            max_weights_v, max_weights_a = None, None
            max_score = 0
            for j in range(0, n_iterations_models):
                som_v = SelfOrganizingMap(n, m, v_dim, n_iterations=n_iters, sigma=sigma, learning_rate=alpha)
                som_a = SelfOrganizingMap(n, m, a_dim, n_iterations=n_iters, sigma=sigma, learning_rate=alpha)
                weights_v, weights_a, _, _ = train_som_and_get_weight(som_v, som_a, v_xs, a_xs, v_ys, a_ys)
                accuracies_test_v, accuracies_test_a = [], []
                accuracies_train_v, accuracies_train_a = [], []
                for n_hebbian_model in range(0, n_iterations_models):
                    hebbian_model = HebbianModel(som_a, som_v, a_dim, v_dim, n_presentations=shots_hebbian_model,
                                                 learning_rate=10,
                                                 n_classes=len(Constants.classes) - 1, threshold=hebbian_threshold)
                    hebbian_model.train(h_a_xs_train_n_1, h_v_xs_train_n_1)

                    # Evaluating Hebbian Model for SOMs with n-1 classes
                    accuracy_test_v = hebbian_model.evaluate(h_a_xs_test_n_1, h_v_xs_test_n_1, h_a_ys_test_n_1,
                                                             h_v_ys_test_n_1, source='v', prediction_alg='regular')
                    accuracy_test_a = hebbian_model.evaluate(h_a_xs_test_n_1, h_v_xs_test_n_1, h_a_ys_test_n_1,
                                                             h_v_ys_test_n_1, source='a', prediction_alg='regular')
                    accuracy_train_v = hebbian_model.evaluate(h_a_xs_train_n_1, h_v_xs_train_n_1, h_a_ys_train_n_1,
                                                              h_v_ys_train_n_1, source='v', prediction_alg='regular')
                    accuracy_train_a = hebbian_model.evaluate(h_a_xs_train_n_1, h_v_xs_train_n_1, h_a_ys_train_n_1,
                                                              h_v_ys_train_n_1, source='a', prediction_alg='regular')
                    accuracies_test_v.append(accuracy_test_v), accuracies_test_a.append(accuracy_test_a)
                    accuracies_train_v.append(accuracy_train_v), accuracies_train_a.append(accuracy_train_a)
                mean_acc_test_v, mean_acc_test_a = np.mean(accuracies_test_v), np.mean(accuracies_test_a)
                mean_acc_train_v, mean_acc_train_a = np.mean(accuracies_train_v), np.mean(accuracies_train_a)
                score_model = mean_acc_test_a + mean_acc_test_v + mean_acc_train_a + mean_acc_train_v
                print(f'Mean Accuracies for model n° {j}')
                print(f'Mean Accuracy on test set from visual: {mean_acc_test_v}')
                print(f'Mean Accuracy on test set from audio: {mean_acc_test_a}')
                print(f'Mean Accuracy on train set from visual: {mean_acc_train_v}')
                print(f'Mean Accuracy on train set from audio: {mean_acc_train_v}')
                print(f'Score model: {score_model} vs. Max Score: {max_score}')
                if score_model > max_score:
                    max_score = score_model
                    max_weights_v, max_weights_a = weights_v, weights_a

            file_path_som_v = os.path.join(inner_path, 'som_v_model.npy')
            file_path_som_a = os.path.join(inner_path, 'som_a_model.npy')
            np.save(file_path_som_v, max_weights_v)
            np.save(file_path_som_a, max_weights_a)

import json
import os
import numpy as np
import seaborn as sb

from oneShotLearning.utility import safe_create_folder
from utils.constants import Constants
import pandas as pd
import matplotlib.pyplot as plt


def autolabel(rects, ax):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


def plot_results_on_bar(data_results, labels_threshold,
                        root_folder=Constants.statistics_path,
                        title=None, type_dataset='training', shots=10):
    if title is None:
        title = ['Plot1', 'Plot2']
    width = 0.10  # the width of the bars
    data_results = np.array(data_results)
    (row, columns) = np.shape(data_results)
    fig, ax = plt.subplots(figsize=(20, 5))
    rects = []
    x = np.arange(columns)  # the label locations
    position = 0
    #colors = sb.color_palette('bright', n_colors=row)
    # print(f'size threshold: {len(labels_threshold)}')
    # print(f'row={row} columns={columns}')
    colors = ['#264653', '#2A9D8F', '#E9C46A', '#F4A261', '#E76F51', '#0075C4']
    for i in range(0, row):
        rect = ax.bar(x + position, data_results[i], width,
                      label=labels_threshold[i], color=colors[i])
        rects.append(rect)
        position += width
    ax.set_ylabel('Accuracy')
    ax.set_title(title)
    ax.set_xticks(x + .25)
    ax.set_xticklabels(Constants.label_classes)
    ax.legend()
    for rect in rects:
        autolabel(rect, ax)
    fig.tight_layout()
    plt.savefig(os.path.join(root_folder, f'plots_{type_dataset}_accuracy_per_class_results.png'))


if __name__ == '__main__':
    threshold = range(60, 90, 5)
    # shots = [1, 5, 10, 15, 20]
    shots = [1, 3, 5, 7, 10, 12, 15, 18]
    result = {}
    for shot in shots:
        result_v_train, result_v_test = [], []
        result_a_train, result_a_test = [], []
        labels_result = []
        thresholds_label = []
        root_folder = os.path.join(Constants.statistics_path, 'final_tests', f'{shot}_shot_accuracies')
        for value_threshold in threshold:
            name_folder = os.path.join('results_threshold_{}'.format(value_threshold))
            name_object_test = 'test_threshold_{}'.format(value_threshold)
            result[name_object_test] = {}
            result[name_object_test]['value_threshold'] = value_threshold
            thresholds_label.append(f'threshold {value_threshold}')
            v_threshold_training_res = []
            v_threshold_test_res = []
            a_threshold_training_res = []
            a_threshold_test_res = []

            for class_index in Constants.classes:
                class_label = Constants.label_classes[class_index]
                df_class = pd.read_csv(os.path.join(root_folder, name_folder,
                                                    '{}_one_shot_class_{}'.format(class_index + 1, class_label),
                                                    'accuracies_per_class.csv'.format(class_label)))
                df_class = df_class.drop("Class", axis=1).drop("Unnamed: 0", axis=1)
                columns = df_class.columns.values
                labels_result.append(columns[0]), labels_result.append(columns[1])
                labels_result.append(columns[2]), labels_result.append(columns[3])
                accuracies = df_class.to_numpy().flatten()
                accuracies = [round(x, 2) for x in accuracies]
                # print(df_class.columns.values)
                # print(df_class.to_numpy().flatten())
                v_threshold_training_res.append(accuracies[0]), a_threshold_training_res.append(accuracies[1])
                v_threshold_test_res.append(accuracies[2]), a_threshold_test_res.append(accuracies[3])
            # print(labels_result)
            result_v_train.append(v_threshold_training_res), result_a_train.append(a_threshold_training_res)
            result_v_test.append(v_threshold_test_res), result_a_test.append(a_threshold_test_res)
        labels_result = list(dict.fromkeys(labels_result))
        # print(labels_result[:2])
        # print(np.array(result_v_train).shape)
        # print(result_a_train)
        # print(thresholds_label)
        train_folder = os.path.join(root_folder, 'training_plots')
        test_folder = os.path.join(root_folder, 'test_plots')
        safe_create_folder(train_folder)
        safe_create_folder(test_folder)
        plot_results_on_bar(result_v_train, labels_threshold=thresholds_label, root_folder=train_folder,
                            title=labels_result[0] + f' <{shot} shots>', type_dataset='training_video', shots=shot)
        plot_results_on_bar(result_a_train, labels_threshold=thresholds_label, root_folder=train_folder,
                            title=labels_result[1] + f' <{shot} shots>', type_dataset='training_audio', shots=shot)
        plot_results_on_bar(result_v_test, labels_threshold=thresholds_label, root_folder=test_folder,
                            title=labels_result[2] + f' <{shot} shots>', type_dataset='test_video', shots=shot)
        plot_results_on_bar(result_a_test, labels_threshold=thresholds_label, root_folder=test_folder,
                            title=labels_result[3] + f' <{shot} shots>', type_dataset='test_audio', shots=shot)

        # with open(
        #         os.path.join(root_folder, name_folder,
        #                      '{}_one_shot_class_{}'.format(class_index + 1, class_label),
        #                      'results_class_{}.md'.format(class_label))) as f:
        #     file_split = [line.split('\n') for line in f]
        #     file_split = [line[0][1:-1].split('|') for line in file_split if 'Accuracy' in line[0]]
        #     for element in file_split:
        #         if result[name_object_test].get(element[0]) is None:
        #             result[name_object_test][element[0]] = [float(element[1])]
        #         else:
        #             result[name_object_test][element[0]].append(float(element[1]))
        # with open(os.path.join(root_folder, 'results_tests.json'), 'w') as f:
        #     json.dump(result, f, indent=True)
        #
        # train_results, test_results = [], []
        # labels_train, labels_test = [], []
        # labels_threshold = []
        #
        # for key, value in result.items():
        #     threshold_val = value.pop('value_threshold', None)
        #     train_result, test_result = [], []
        #     labels_threshold.append(key)
        #     for key_inner, value_inner in value.items():
        #         mean = round(np.mean(value_inner), 2)
        #         if 'Training' in key_inner:
        #             train_result.append(mean), labels_train.append(key_inner)
        #         else:
        #             test_result.append(mean), labels_test.append(key_inner)
        #     train_results.append(train_result), test_results.append(test_result)
        #
        # train_results, test_results = np.array(train_results), np.array(test_results)
        # labels_train, labels_test = list(dict.fromkeys(labels_train)), list(dict.fromkeys(labels_test))
        # plot_results_on_bar(train_results, labels_train, labels_threshold, root_folder=root_folder, data_type='Training')
        # plot_results_on_bar(test_results, labels_test, labels_threshold, root_folder=root_folder, data_type='Test')
